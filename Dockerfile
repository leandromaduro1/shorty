FROM ruby:2.5

WORKDIR /shorten

ADD . .

RUN gem install bundler && bundle

ENV PUMA_MIN_THREAD 0
ENV PUMA_MAX_THREAD 16
ENV PUMA_WORKER_TIMEOUT 60
ENV RACK_ENV production

EXPOSE 9292

CMD ["sh", "-c", "bundle exec rake migrate && bundle exec puma"]

