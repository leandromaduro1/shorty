# Shorty Challenge - Impraise

Shorten is a small service responsible for shortening URL
It was developed with [Sinatra](http://sinatrarb.com/) and [Cassandra](ssandra.apache.org)

## Install

```
        git clone git@gitlab.com:leandromaduro1/shorty.git
        cd shorty
        docker-compose up
```
### Using

`POST /shorten` - Creates a shortcode

```
        curl -XPOST -H "Content-type: application/json" -d '{
                "url": "https://www.google.com",
                "shortcode": ""
        }' 'http://localhost/shorten' | jq

```

```
        { "shortcode": "42e971" }
```

`GET /:shortcode` - Retrieves and redirects to the persisted URL


`GET /:shortcode/stat` - Retrieves all information related to informed shortcode
```
        curl -XGET -H "Content-type: application/json" 'http://localhost/42e971/stat' | jq
```
```
        {
                "startDate": "2018-07-29T21:48:39+00:00",
                "redirectCount": 4,
                "lastSeenDate": "2018-07-29T21:53:15+00:00"
        }
```

### Running tests

```
        docker-compose run --rm shorten bash -c "bundle exec rake migrate && rspec"
```
