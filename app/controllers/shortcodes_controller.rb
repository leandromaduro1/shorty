require 'sinatra/base'
require 'sinatra/json'

class ShortcodesController < Sinatra::Base
  get '/:shortcode', provides: :json do
    service = RetrieveShortcode.find(params[:shortcode])
    service.increment

    status 302
    headers "Location" => service.shortcode.url
  end

  get '/:shortcode/stat', provides: :json do
    service = RetrieveShortcode.find(params[:shortcode])

    status 200
    json(service.shortcode.hashable)
  end

  post '/shorten', provides: :json do
    hashable_params = JSON.parse(request.body.read, symbolize_names: true)
    shortcode = Shortcode.new(url: hashable_params[:url], code: hashable_params[:shortcode])
    CreateShortcode.new(shortcode).perform!

    status 201
    json({shortcode: shortcode.code})
  end

  error JSON::ParserError do
    halt 400, {description: 'Invalid JSON'}.to_json
  end

  error CreateShortcode::URLNotPresent do
    halt 400, {description: 'url not present'}.to_json
  end

  error CreateShortcode::InvalidShortcode do
    halt 422, {description: 'wrong shortcode'}.to_json
  end

  error CreateShortcode::ShortcodeAlreadyTaken do
    halt 409, {description: 'shortcode is already in use'}.to_json
  end

  error RetrieveShortcode::NotFound do
    halt 404, {description: 'shortcode not found'}.to_json
  end
end
