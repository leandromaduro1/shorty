class Shortcode
  attr_accessor :code, :url, :redirect_count, :created_at, :updated_at

  def initialize(attributes={})
    attributes.each_pair{|k,v| send("#{k}=", v)}

    @code = generate_code if code.to_s.empty?
  end

  def hashable
    hash = {
      startDate: created_at.iso8601,
      redirectCount: redirect_count
    }

    hash[:lastSeenDate] = updated_at.iso8601 if redirect_count > 0

    hash
  end

  private

  def generate_code
    Digest::MD5.hexdigest(Time.now.to_s)[0..5]
  end
end
