class ShortcodeRepository
  # Retrieves shortcode
  #
  # @param code [String] shortcode
  # @return [Shortcode] || nil
  # @see Shortcode
  def self.find(code)
    CassandraRepository.find(Shortcode, :code, code)
  end

  # Increments by 1 the number of redirects
  #
  # @param shortcode [Shortcode]
  def self.increment_redirect_count(shortcode)
    CassandraRepository.increment_counter(shortcode, :redirect_count, :code)
  end

  # Persists a shortcode into the database
  #
  # @param shortcode [Shortcode]
  def self.create(shortcode)
    shortcode.redirect_count = 0
    CassandraRepository.create(shortcode)
  end


  # Returns the number of persisted elements
  #
  # @return [Integer]
  def self.count
    CassandraRepository.count(Shortcode)
  end
end
