class CreateShortcode
  class URLNotPresent < StandardError; end;
  class ShortcodeAlreadyTaken < StandardError; end;
  class InvalidShortcode < StandardError; end;

  SHORTCODE_REGEX = /^[0-9a-zA-Z_]{4,}$/

  def initialize(shortcode)
    @shortcode = shortcode
  end

  def perform!
    validate!
    ShortcodeRepository.create(@shortcode)
  end

  private

  def validate!
    raise URLNotPresent.new('url is not present') if @shortcode.url.to_s.strip.empty?
    raise InvalidShortcode.new("The shortcode fails to meet the following regexp: #{SHORTCODE_REGEX}") unless SHORTCODE_REGEX.match?(@shortcode.code)
    raise ShortcodeAlreadyTaken.new('The the desired shortcode is already in use') unless ShortcodeRepository.find(@shortcode.code).nil?
  end
end
