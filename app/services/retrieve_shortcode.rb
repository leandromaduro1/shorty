class RetrieveShortcode
  class NotFound < StandardError; end;

  attr_reader :shortcode

  def initialize(shortcode)
    @shortcode = shortcode
  end

  def self.find(code)
    shortcode = ShortcodeRepository.find(code)
    raise NotFound.new('not found') if shortcode.nil?

    new(shortcode)
  end

  def increment
    ShortcodeRepository.increment_redirect_count(shortcode)
  end
end
