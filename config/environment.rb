require 'bundler'

$LOAD_PATH.unshift(File.expand_path('../../', __FILE__))

ENV['RACK_ENV'] ||= 'development'

Bundler.require :default, ENV['RACK_ENV'].to_sym

require 'lib/cassandra_repository'
require 'app/entities/shortcode'
require 'app/repositories/shortcode_repository'
require 'app/services/create_shortcode'
require 'app/services/retrieve_shortcode'
require 'app/controllers/shortcodes_controller'
