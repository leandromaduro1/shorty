require 'cassandra'
require 'active_support/inflector'

class CassandraRepository
  def self.session
    @@session ||= create_session
  end

  # Increments the specified counter by 1
  #
  # @param entity [Object] target object
  # @param counter [String|Symbol] field which will be incremented
  # @param key [String|Symbol] primary key table
  def self.increment_counter(entity, counter, key)
    table = entity.class.name.pluralize.downcase

    update_statement = <<-UPDATE_CQL
      UPDATE #{table}
      SET #{counter} = ?, updated_at = toTimestamp(now())
      WHERE #{key} = ?
    UPDATE_CQL

    session.execute(session.prepare(update_statement), arguments: [entity.send(counter)+1, entity.send(key)])
  end

  # Retrieves the object from the database
  #
  # @param klass [Class] target table
  # @param key [String|Symbol] primary key table
  # @param value [Object] value used to search the object in the database
  # @return [Object<klass>|nil]
  def self.find(klass, key, value)
    table = klass.name.pluralize.downcase

    statement = session.prepare("SELECT * FROM #{table} WHERE #{key} = ?")
    result = session.execute(statement, arguments: [value])
    result.first.nil? ? nil : klass.new(result.first)
  end

  # Pesists the object into the database
  #
  # It uses the object to determine which table store the information
  # @param entity [Object]
  def self.create(entity)
    table = entity.class.name.pluralize.downcase
    attributes = entity.instance_variables.map{|n| n[1..-1]}.reject{|n| n['created_at'] || n['updated_at']}

    insert_statement = <<-INSERT_CQL
      INSERT INTO #{table}
      (#{attributes.join(', ')}, created_at, updated_at)
      VALUES
      (#{attributes.map{|_| '?'}.join(', ')}, toTimestamp(now()), toTimestamp(now()))
    INSERT_CQL

    session.execute(session.prepare(insert_statement), arguments: attributes.map{|n| entity.send(n)})
    true
  end

  # Returns the total number of persisted element in the specified table
  #
  # @param klass [Class]
  # @return [Integer]
  def self.count(klass)
    table = klass.name.pluralize.downcase
    session.execute("SELECT COUNT(1) AS total FROM #{table}").first['total']
  end

  def self.create_session
    cluster = Cassandra.cluster(
      username: ENV['CASSANDRA_USERNAME'] || '',
      password: ENV['CASSANDRA_PASSWORD'] || '',
      hosts:    ENV['CASSANDRA_HOST'].split(","),
      port:     ENV['CASSANDRA_PORT'].to_i
    )
    session = cluster.connect('system')

    keyspace_definition = <<-KEYSPACE_CQL
      CREATE KEYSPACE IF NOT EXISTS #{ENV['CASSANDRA_KEYSPACE']}
      WITH replication = {
        'class': 'SimpleStrategy',
        'replication_factor': 3
      }
    KEYSPACE_CQL

    session.execute(session.prepare(keyspace_definition))
    session.execute(session.prepare("USE #{ENV['CASSANDRA_KEYSPACE']}"))

    session
  end

  private_class_method :create_session
end
