require 'spec_helper'

describe ShortcodesController, type: :request do
  let(:headers) {{'Content-Type' => 'application/json'}}

  context 'POST /shorten' do
    subject(:perform) { post '/shorten', {url: url, shortcode: code}.to_json, headers }

    let(:url)  { 'www.google.com' }
    let(:code) { 'ireland123' }

    it 'returns a valid json' do
      perform

      expect(json_response).to eq({shortcode: 'ireland123'})
    end

    it 'returns 201' do
      perform

      expect(last_response.status).to eq(201)
    end

    context 'when URL is not informed' do
      let(:url)  { nil }
      let(:code) { nil }

      it 'returns 400' do
        perform

        expect(last_response.status).to eq(400)
      end
    end

    context 'when code is already in use' do
      let(:url)  { 'www.google.com' }
      let(:code) { 'IRELAND123' }

      before do
        CreateShortcode.new(Shortcode.new({url: url, code: code})).perform!
      end

      it 'returns 409' do
        perform

        expect(last_response.status).to eq(409)
      end
    end

    context 'when code is invalid' do
      let(:url)  { 'www.google.com' }
      let(:code) { 'IRE' }

      it 'returns 422' do
        perform

        expect(last_response.status).to eq(422)
      end
    end

    context 'when code is the same but with capital letter' do
      let(:url)  { 'www.google.com' }
      let(:code) { 'ireland123' }

      before do
        CreateShortcode.new({url: url, code: 'IRELAND123'})
      end

      it 'returns 201' do
        perform

        expect(last_response.status).to eq(201)
      end
    end
  end

  context 'GET /:shortcode' do
    subject(:perform) { get "/#{code}", {}, headers }

    let(:code) { 'down123' }
    let(:url)  { 'www.google.com' }

    before do
      CreateShortcode.new(Shortcode.new({url: url, code: 'down123'})).perform!
    end

    it 'returns 302' do
      perform

      expect(last_response.status).to eq(302)
    end

    it 'returns the correct location' do
      perform

      expect(last_response.headers['Location']).to eq(url)
    end

    it 'increments by 1 redirect count' do
      expect(ShortcodeRepository.find(code).redirect_count).to eq(0)
      perform
      expect(ShortcodeRepository.find(code).redirect_count).to eq(1)
    end

    context 'when code does not exist' do
      let(:code) { 'down12345' }
      it 'returns 404' do
        perform

        expect(last_response.status).to eq(404)
      end
    end
  end

  context 'GET /:shortcode/stat' do
    subject(:perform) { get "/#{code}/stat", {}, headers }

    before do
      CreateShortcode.new(Shortcode.new({url: url, code: 'down123'})).perform!
      allow_any_instance_of(Shortcode).to receive(:created_at) { time }
      allow_any_instance_of(Shortcode).to receive(:updated_at) { time }
    end

    let(:time) { Time.parse('2018-07-29 20:52:40') }
    let(:code) { 'down123' }
    let(:url)  { 'www.google.com' }
    let(:expected_result) do
      {
        startDate: time.iso8601,
        redirectCount: 0
      }
    end

    it 'returns 200' do
      perform

      expect(last_response.status).to eq(200)
    end

    it 'returns the correct information' do
      perform

      expect(json_response).to eq(expected_result)
    end

    context 'when redirect_count is greater than 0' do
      before do
        get "/#{code}", {}, headers
      end

      let(:expected_result) do
        {
          startDate: time.iso8601,
          redirectCount: 1,
          lastSeenDate: time.iso8601
        }
      end

      it 'returns the correct information' do
        perform

        expect(json_response).to eq(expected_result)
      end
    end

    context 'when code does not exist' do
      let(:code) { 'down12345' }
      it 'returns 404' do
        perform

        expect(last_response.status).to eq(404)
      end
    end
  end
end
