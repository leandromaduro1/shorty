require 'spec_helper'

describe Shortcode do
  describe '#initialize' do
    subject(:shortcode) { described_class.new({url: '123', code: nil}) }

    context 'when code is not informed' do
      it 'generates a code' do
        expect(shortcode.code).to_not be_nil
      end

      it 'generates a code of length 6' do
        expect(shortcode.code.length).to eq(6)
      end
    end
  end
end
