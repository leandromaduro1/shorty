require 'spec_helper'

describe ShortcodeRepository do
  let(:shortcode) { Shortcode.new(url: 'www.google.com', code: nil) }

  describe '.create' do
    subject(:create) { described_class.create(shortcode) }

    it 'persists the object into the database' do
      expect{create}.to change{ShortcodeRepository.count}.by(1)
    end
  end

  describe '.find' do
    before do
      ShortcodeRepository.create(shortcode)
    end

    subject(:find) { described_class.find(code) }

    let(:code) { shortcode.code }

    context 'when code does not exists' do
      let(:code) { "#{shortcode.code}123213" }

      it 'returns nil' do
        expect(find).to be_nil
      end
    end

    it 'returns the correct object' do
      obj = find

      expect(obj.url).to eq(shortcode.url)
    end
  end

  describe '.increment_redirect_count' do
    before do
      ShortcodeRepository.create(shortcode)
    end

    subject(:increment) { described_class.increment_redirect_count(shortcode) }

    it 'updates the redirect count' do
      expect(ShortcodeRepository.find(shortcode.code).redirect_count).to eq(0)
      increment
      expect(ShortcodeRepository.find(shortcode.code).redirect_count).to eq(1)
    end
  end
end
