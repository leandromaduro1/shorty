require 'spec_helper'

describe CreateShortcode do
  describe '#perform!' do
    before do
      allow(ShortcodeRepository).to receive(:create) { true }
      allow(ShortcodeRepository).to receive(:find)   { nil }
    end

    let(:shortcode) { Shortcode.new(code: code, url: url) }
    let(:code) { 'HSUD6363' }
    let(:url)  { 'www.google.com' }

    subject(:perform) { described_class.new(shortcode).perform! }

    it 'persists in the database' do
      expect(ShortcodeRepository).to receive(:create).once

      perform
    end

    context 'when the URL is not informed' do
      let(:url)  { nil }
      it 'raises CreateShortcode::URLNotPresent exception' do
        expect{perform}.to raise_error(CreateShortcode::URLNotPresent, 'url is not present')
      end
    end

    context 'when the URL is empty' do
      let(:url)  { '' }
      it 'raises CreateShortcode::URLNotPresent expection' do
        expect{perform}.to raise_error(CreateShortcode::URLNotPresent, 'url is not present')
      end
    end

    context 'when the URL is filled with spaces' do
      let(:url)  { '   ' }
      it 'raises CreateShortcode::URLNotPresent exception' do
        expect{perform}.to raise_error(CreateShortcode::URLNotPresent, 'url is not present')
      end
    end

    context 'when the shortcode is not valid' do
      let(:code) { 'he1' }
      it 'raises CreateShortcode::InvalidShortcode exception' do
        expect{perform}.to raise_error(CreateShortcode::InvalidShortcode, "The shortcode fails to meet the following regexp: #{CreateShortcode::SHORTCODE_REGEX}")
      end
    end

    context 'when the shortcode is already taken' do
      before do
        allow(ShortcodeRepository).to receive(:find) { Shortcode.new(url: 1, code: nil) }
      end

      it 'raises CreateShortcode::ShortcodeAlreadyTaken exception' do
        expect{perform}.to raise_error(CreateShortcode::ShortcodeAlreadyTaken, 'The the desired shortcode is already in use')
      end
    end
  end
end
