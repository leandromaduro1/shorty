ENV['RACK_ENV'] = 'test'

require 'simplecov'
require 'rack/test'
require 'rake'
require './config/environment'

SimpleCov.start

def app
  ShortcodesController
end

load('Rakefile')
Dir['spec/support/**/*.rb'].each {|f| require f}

RSpec.configure do |config|
  config.include Rack::Test::Methods
  config.include JSONHelper

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.around(:each, type: :request) do |example|
    Rake::Task['reset_keyspace'].execute
    example.run
  end
end
